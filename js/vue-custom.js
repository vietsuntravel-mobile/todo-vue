
$(document).ready(function () {
    Vue.component('select-tab', {
        props: ['todo'],
        template: `
        <li class="sub-menu-main">
            <a v-bind:href="todo.url">
            <p>
            <i v-bind:class="todo.faicon"></i>
            <span>{{ todo.nametabs }}</span>
            </p>
            <p class="number-main">{{ todo.numbertab }}</p>
            </a>
        </li >`
    });
    new Vue({
        el: '#btn-tabs',
        data: {
            selectTab: [{
                id: 0,
                nametabs: "My day",
                numbertab: "3",
                url: "#tab1",
                faicon: "far fa-sun",
            },
            {
                id: 1,
                nametabs: "Important",
                numbertab: "3",
                url: "#tab2",
                faicon: "far fa-star",
            },
            {
                id: 2,
                nametabs: "Planner",
                numbertab: "3",
                url: "#tab3",
                faicon: "fas fa-calendar-alt",
            },
            {
                id: 3,
                nametabs: "Task",
                numbertab: "3",
                url: "#tab4",
                faicon: "fas fa-home",
            },
            ]
        },
    });
    Vue.component('user-login', {
        props: ['todo'],
        template: `
    <div class="media">
    <div class="media-left">
        <img v-bind:src="todo.image" class="avatar-forum">
    </div>
    <div class="media-body media-middle">
        <span>{{todo.nameID}}</span>
    </div>
    </div> `
    });
    new Vue({
        el: '#user-login',
        data: {
            userlogin: [{
                image: "media/avatar.jpg",
                nameID: "Le Huu Phuc",
            },
            ]
        },
    });


    
    new Vue({
        el: '#taball',
        data: {
            tabMain: [{
                id: 0,
                idmain: "tab1",
                namemenuMain: "My Day",
                namemenuSub: "Them swager-ui-restify làm exporer 1",
                addatask: "Add a task",
                imageheader: "background: url(media/image-slider-2.jpg) no-repeat ; background-position: center center;background-size: cover ",
            },
            {
                id: 1,
                idmain: "tab2",
                namemenuMain: "My Day 2",
                namemenuSub: "Them swager-ui-restify làm exporer 2",
                addatask: "Add a task",
                imageheader: "background: url(media/image-slider-2.jpg) no-repeat ; background-position: center center;background-size: cover ",
            },
            {
                id: 2,
                idmain: "tab3",
                namemenuMain: "My Day 3",
                namemenuSub: "Them swager-ui-restify làm exporer 3",
                addatask: "Add a task due today",
                imageheader: "background: url(media/image-slider-2.jpg) no-repeat ; background-position: center center;background-size: cover ",
            }, {
                id: 3,
                idmain: "tab4",
                namemenuMain: "My Day 4",
                namemenuSub: "Them swager-ui-restify làm exporer 4",
                addatask: "Add a task",
                imageheader: "background: url(media/image-slider-2.jpg) no-repeat ; background-position: center center;background-size: cover ",
            },

            ]
        },
       
    });





new Vue({
    el: '#newlist',
    data: {
        todos: [
            { text: 'Learn JavaScript',
           
         },
            { text: 'Learn Vue' },
           
        ],
        newTodo: ''
    },
    computed: {
        totalTodos() {
            return this.todos.length;
        }
    },
    methods: {
        deleteTodo(index) {
            this.todos.splice(index, 1);
        },
        addTodo() {
            if(this.newTodo == "") return;
            this.todos.push({
                text: this.newTodo
            });
            this.newTodo = '';
        }
    }
});








new Vue({
    el: '#newlist1',
    data: {
        todoss: [
            {
                idman: 1,
                text: 'Learn JavaScript2',
           
         },
            { 
                idman: 2,
                text: 'Learn Vue2' },
           
        ],
        
        newTodos: ''
    },
    computed: {
        totalTodoss() {
            return this.todoss.length;
        }
    },
    methods: {
        deleteTodos(index) {
            this.todoss.splice(index, 1);
          
        },
        deleteTodo1(newl) {
           
            this.todoss.splice(this.newl);
        },


        addTodos() {

            if(this.newTodos == "") return;

            this.todoss.push({
                text: this.newTodos
            });
            this.newTodos = '';
        }
    },
})

// newlist1(newlist)

;


new Vue({
    el: '#newlisttab2',
    data: {
        todos2: [
            { text: 'Learn JavaScript2',
         },        
        ],
        newTodo2: ''
    },
    computed: {
        totalTodos2() {
            return this.todos2.length;
        }
    },
    methods: {
        deleteTodo2:function(index) {
            this.todos2.splice(index, 1);
        },
        addTodo2() {
            if(this.newTodo2 == "") return;
            this.todos2.push({
                text: this.newTodo2
            });
            this.newTodo2 = '';
        }
    }
});


  



//   new Vue({
//     el: '#checkBox',
//     data: {
    
//       isTrue: true,
    
//     }
//   })
  







});

