$(document).ready(function () {
    $('ul.tabs').each(function () {
        // For each set of tabs, we want to keep track of
        // which tab is active and it's associated content
        var $active, $content, $links = $(this).find('a');

        // If the location.hash matches one of the links, use that as the active tab.
        // If no match is found, use the first link as the initial active tab.
        $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
        $active.addClass('active');
        $content = $($active.attr('href'));

        // Hide the remaining content
        $links.not($active).each(function () {
            $($(this).attr('href')).hide();
        });

        // Bind the click event handler
        $(this).on('click', 'a', function (e) {
            // Make the old tab inactive.
            $active.removeClass('active');
            $content.hide();

            // Update the variables with the new link and content
            $active = $(this);
            $content = $($(this).attr('href'));

            // Make the tab active.
            $active.addClass('active');
            $content.show();

            // Prevent the anchor's default click action
            e.preventDefault();
        });
    });
});

$(document).ready(function () {
    //open popup
    $('.cd-popup-trigger').on('click', function (event) {
        event.preventDefault();
        $('.cd-popup').addClass('is-visible');
    });

    //close popup
    $('.cd-popup').on('click', function (event) {
        if ($(event.target).is('.cd-popup-close')) {
            event.preventDefault();
            $(this).removeClass('is-visible');
        }
    });
    //close popup when clicking the esc keyboard button
    $(document).keyup(function (event) {
        if (event.which == '27') {
            $('.cd-popup').removeClass('is-visible');
        }
    });
});

$(document).ready(function () {
    //open popup
    $('.cd-popup-option-show').on('click', function (event) {
        event.preventDefault();
        $('.cd-popup-option').addClass('is-visible');
    });

    //close popup
    $('.cd-popup-option').on('click', function (event) {
        if ( $(event.target).is('.cd-popup-option')) {
            event.preventDefault();
            $(this).removeClass('is-visible');
        }
    });
    //close popup when clicking the esc keyboard button
    $(document).keyup(function (event) {
        if (event.which == '27') {
            $('.cd-popup-option').removeClass('is-visible');
        }
    });
});



function add_child() {
    var splice = document.getElementById("delete-list-sub");

    splice.parentNode.removeChild(splice);
   
              }
///-------------------------------------------------------------

              function add_child2() {
    var splice = document.getElementById("delete-list-sub2");
    splice.parentNode.removeChild(splice);
              }


// $(document).ready(function () {
//     $('.showuser').click(function () {
//         $('.user-id').slideToggle("fast");
//     });
// });

// $(document).ready(function () {
//     $("li").click(function () {

//         $("li").removeClass("click");
//         $(this).addClass("click");
//     });
// });










